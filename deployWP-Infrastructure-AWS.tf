#Infrastructure as a CODE. 
#
# What we are going to do, is deploying the whole infrasestrucutre, incluing EC2, VPC, RDS, to set up a new Wordpress SITE. There is another file attached called "user-data".
#
# Author: Miguel Angel Fernandez
# Date: 10th February 2018



#We are going to set up the provider, in this case  ['AWS'] and then the keys and region.

provider "aws" {
  access_key = "${var.access_key}"
  secret_key = "${var.secret_access_key}"
  region     = "eu-west-3"
}

#We setup the cidr block that we like.
resource "aws_vpc" "masip" {
  cidr_block = "10.0.0.0/16"
}

resource "aws_subnet" "public-masip-3a" {
  vpc_id     = "${aws_vpc.masip.id}"
  cidr_block = "10.0.1.0/24"
  availability_zone = "eu-west-3a"
}

resource "aws_subnet" "public-masip-3b" {
  vpc_id     = "${aws_vpc.masip.id}"
  cidr_block = "10.0.2.0/24"
  availability_zone = "eu-west-3b"
}

resource "aws_subnet" "public-masip-3c" {
  vpc_id     = "${aws_vpc.masip.id}"
  cidr_block = "10.0.3.0/24"
  availability_zone = "eu-west-3c"
}
resource "aws_subnet" "private-masip-3a" {
  vpc_id     = "${aws_vpc.masip.id}"
  cidr_block = "10.0.4.0/24"
  availability_zone = "eu-west-3c"
}

resource "aws_internet_gateway" "gw" {
  vpc_id = "${aws_vpc.masip.id}"
}

resource "aws_route_table" "public-masip" {
  vpc_id = "${aws_vpc.masip.id}"
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.gw.id}"
  }
}
resource "aws_route_table_association" "a" {
  subnet_id      = "${aws_subnet.public-masip-3a.id}"
  route_table_id = "${aws_route_table.public-masip.id}"
}
resource "aws_route_table_association" "b" {
  subnet_id      = "${aws_subnet.public-masip-3b.id}"
  route_table_id = "${aws_route_table.public-masip.id}"
}
resource "aws_route_table_association" "c" {
  subnet_id      = "${aws_subnet.public-masip-3c.id}"
  route_table_id = "${aws_route_table.public-masip.id}"
}

#We should configure the security group. 
resource "aws_security_group" "WORDPRESS" {
  description = "SG-WORDPRESS"

  ingress {
      from_port = 22
      to_port = 22
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
  }
   ingress {
      from_port = 80
      to_port = 80
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
      from_port = 0
      to_port = 0
      protocol = "-1"
      cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${aws_vpc.masip.id}"
}

#This is the data that is going to be launch when the instance is ready. We need the Endpoint url from RDS, so we use it as a var.
resource "template_file" "user-data" {
    filename = "${file("user-data.txt")}"
    vars {
    	end_point_rds = "${aws_db_instance.db.endpoint}"
  	}
}

resource "aws_instance" "web" {
  ami = "ami-8ee056f3"
  associate_public_ip_address = true
  instance_type = "t2.micro"
  user_data = "${template_file.user-data.rendered}"
  key_name = "testing"
  subnet_id = "${aws_subnet.public-masip-3c.id}"
  tags {
    Name = "Wordpress"
  }
  vpc_security_group_ids = [ "${aws_security_group.WORDPRESS.id}" ]
  depends_on = ["aws_db_instance.db"]

}

resource "aws_db_instance" "db" {
  allocated_storage    = 10
  storage_type         = "gp2"
  engine               = "mysql"
  engine_version       = "5.6.37"
  instance_class       = "db.t2.micro"
  name                 = "mydb"
  username             = "wordpress"
  password             = "MyPasswordIsFour"
  parameter_group_name = "default.mysql5.6"
  publicly_accessible = true
  skip_final_snapshot = true

}