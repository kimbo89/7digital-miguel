# 7Digital-Miguel

Infrastructure-As-A-Code

1.Launch the infrastructure.

We are going to use Terraform as the tool to deploy the arquitecture. We have been provided all the configuration files that we need. In this case we are going to need 2 of them.
	-	deployWP-Infrastructure-AWS.tf
	-	user-data.txt
	
To deploy the app, we need to have installed Terraform, if we haven’t we should have a look to the following link. 

	https://www.terraform.io/intro/getting-started/install.html
	
We should put both files in the same directory than terraform. When we have them, we should launch the following commands:

	terraform plan

/* We can check what terraform is going to deploy. */

	terraform apply –var ‘access-key=[OUR_ACCESS_KEY]’ –var ‘secret_access_key=[OUR_SECRET_ACCES_KEY] ‘

/* We deploy all the changes into AWS. We have to put our credentials */

2.Scaling the services.

As I like always to have the same resources to deploy the arquitecture, I configured manually both types of instances, the ec2 one, and the rds one.  Inside the deployWP-Infrastructure-AWS.tf file, we can find two options:

	resource "aws_instance" "web"{
	........
	  instance_type = “t2.micro”
	........
	}
	resource "aws_db_instance" "db" {
	........
	 instance_class       = “db.t2.micro”
	........
	}	

But, we have also the opportunity to make it variable and send the type at the same time that we launch the full command:

	resource "aws_instance" "web"{
	........
	  instance_type = "${var.instance_type_web}" 
	........
	}
	resource "aws_db_instance" "db" {
	........
	 instance_class       = "${var.instance_type_rds}" 
	........
	}

	terraform apply –var ‘access-key=[OUR_ACCESS_KEY]’ –var ‘secret_access_key=[OUR_SECRET_ACCES_KEY] ‘ –var ‘instance_type_web=t2.micro –var ‘instance_type_rds=db.t2.micro’
/*We deploy all the changes into AWS. We have to put or credentials, and both types of instances */

Just in case that with even changing the EC2 instances and RDS instance we can’t support all the audience, we have to change the infrastructure, and configure a new ELB and Auto scaling group to provide service with several instances.

